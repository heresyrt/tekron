package com.tekron.homepage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TekronApplication {

	public static void main(String[] args) {
		SpringApplication.run(TekronApplication.class, args);
	}

}
